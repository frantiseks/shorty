package main

import (
	"flag"
	"github.com/boltdb/bolt"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
	"log"
	"os"
)

const DATABASE_FILE = "storage.db"

func main() {
	db, err := bolt.Open(DATABASE_FILE, 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	storage, err := NewStorage(db)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	api := &Api{storage}

	var port = flag.String("p", "4000", "http port")
	flag.Parse()

	e := echo.New()
	e.Use(mw.Logger())
	e.Use(mw.Recover())

	e.Post("/", api.ShortenUrl)
	e.Get("/:shortUrl", api.GetUrl)

	e.Run(":" + *port)

}
