package main

import (
	"fmt"
	"github.com/boltdb/bolt"
)

const URL_BUCKET = "urls"

type Storage struct {
	db *bolt.DB
}

func NewStorage(db *bolt.DB) (*Storage, error) {
	err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(URL_BUCKET))

		if err != nil {
			return err
		}
		return nil
	})

	return &Storage{db}, err
}

func (storage *Storage) SaveUrl(longUrl string, shortUrl string) error {
	err := storage.db.Update(func(tx *bolt.Tx) error {
		urls := tx.Bucket([]byte(URL_BUCKET))
		if err := urls.Put([]byte(shortUrl), []byte(longUrl)); err != nil {
			return err
		}
		return nil
	})

	return err
}

func (storage *Storage) GetUrl(shortUrl string) (string, error) {
	var result = ""
	err := storage.db.View(func(tx *bolt.Tx) error {
		urls := tx.Bucket([]byte(URL_BUCKET))
		if urls == nil {
			return fmt.Errorf("urls %s not found", URL_BUCKET)
		}

		result = string(urls.Get([]byte(shortUrl)))
		return nil
	})

	return result, err
}
