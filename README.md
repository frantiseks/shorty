# README #

Simple url shortner implementation in Go. Http endpoints are implemented via echo framework. As embedded storage is used bolt db. 

# Usage #
Start compiled binary with argument:

```
#!bash

./shorty -p 80
```
Shorten url is created via POST request on root context "/" e.g:

```
#!bash

 http localhost LongUrl=http://www.example.com
```

```
#!http

HTTP/1.1 201 Created
Content-Length: 52
Content-Type: application/json; charset=utf-8
Date: Sun, 24 Jan 2016 10:49:55 GMT

{
    "LongUrl": "http://www.example.com", 
    "ShortUrl": "8174a7"
}

```
Short url can be used as follows:

```
#!bash

http://localhost/8174a7
```
Request is automatically redirected to long target url.