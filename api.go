package main

import (
	"github.com/labstack/echo"
	"github.com/satori/go.uuid"
	"net/http"
	"net/url"
	"strings"
)

type UrlDto struct {
	LongUrl  string
	ShortUrl string
}

type Api struct {
	storage *Storage
}

func (api Api) ShortenUrl(c *echo.Context) error {
	dto,err := processUrlDto(c)
	if(err != nil){
		return err
	}

	shortUrl := createRandomUrlKey()
	if err := api.storage.SaveUrl(dto.LongUrl, shortUrl); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusCreated, &UrlDto{dto.LongUrl, shortUrl})
}

func (api Api) GetUrl(c *echo.Context) error {
	shortUrl := c.Param("shortUrl")

	longUrl, err := api.storage.GetUrl(shortUrl)
	if err != nil {
		return err
	}

	if longUrl == "" {
		return echo.NewHTTPError(http.StatusNotFound)
	}

	return c.Redirect(http.StatusFound, longUrl)
}

func createRandomUrlKey() string {
	var uuid = uuid.NewV4().String()
	uuid = strings.Replace(uuid, "-", "", 0)
	return uuid[0:6]
}

func processUrlDto(c *echo.Context) (*UrlDto, error) {
	dto := &UrlDto{}

	if err := c.Bind(dto); err != nil {
		return dto, echo.NewHTTPError(http.StatusBadRequest)
	}

	if dto.LongUrl == "" {
		return dto, echo.NewHTTPError(http.StatusBadRequest)
	}

	if _, err := url.Parse(dto.LongUrl); err != nil {
		return dto, echo.NewHTTPError(http.StatusBadRequest)
	}

	return dto, nil
}
