package main

import (
	"fmt"
	"github.com/boltdb/bolt"
	"io/ioutil"
	"testing"
)

func TestNewStorage(t *testing.T) {
	var db = prepareDatabase(t)
	_, err := NewStorage(db)
	defer db.Close()

	if err != nil {
		fmt.Println(err)
		t.Fatal("Internal storage was not created")
	}
}

func TestSaveUrl(t *testing.T) {
	var db = prepareDatabase(t)
	storage, err := NewStorage(db)
	defer db.Close()

	var longUrl = "www.example.com"
	var shortUrl = "abcdef"
	err = storage.SaveUrl(longUrl, shortUrl)
	if err != nil {
		t.Fatal("Unable to save url")
	}

	fetchedUrl, err := storage.GetUrl(shortUrl)
	if longUrl != fetchedUrl {
		t.Fatalf("fetched url %s is different from reference url %s", fetchedUrl, longUrl)
	}

}

func TestGetUrl(t *testing.T) {
	var db = prepareDatabase(t)
	storage, err := NewStorage(db)
	defer db.Close()

	var longUrl = "www.example.com"
	var shortUrl = "efghji"
	err = storage.SaveUrl(longUrl, shortUrl)
	if err != nil {
		t.Fatal("Unable to save url")
	}

	_, err = storage.GetUrl(shortUrl)
	if err != nil {
		t.Fatalf("Unable get long url for short url %s", shortUrl)
	}

}

func prepareDatabase(t *testing.T) *bolt.DB {
	file, err := ioutil.TempFile("", "shorty")
	db, err := bolt.Open(file.Name(), 0600, nil)
	if err != nil {
		t.Fatal("Internal to open connection to bolt")
	}

	return db
}
